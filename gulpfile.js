var gulp =          require('gulp');

// Browser
var browsersync =   require('browser-sync').create();

// CSS
var sass =          require('gulp-sass');
var autoprefixer =  require('gulp-autoprefixer');

// JS
var uglify =        require('gulp-uglify');
var babelify =      require('babelify');
var browserify =    require('browserify');
var source =        require('vinyl-source-stream');
var buffer =        require('vinyl-buffer');

// Twig
var twig =          require('gulp-twig');
var twigData =      require('./src/json/twig-data.json');

// Utility
var rename  =       require('gulp-rename');
var sourcemaps =    require('gulp-sourcemaps');
var plumber =       require('gulp-plumber');
var cleanFiles =    require('gulp-clean');

var path = {
  server: './dist/',
  src: {
    css:                  'src/**/*.css',
    scss:                 'src/sass/**/*.scss',
    es6:                  'src/js/**/*.es6',
    fonts:                'src/fonts/**/*',
    html:                 'src/*.html',
    images:               'src/images/**/*.*',
    imagesFolderFiles:    'src/images/**/*',
    js:                   'src/js/**/*.js',
    jsRoot:               'src/js/',
    jsApp:                'app.es6',
    root:                 "./src",
    styleguide: {
      css:                "src/styleguide/examples/**/*.css",
      docsAssets:         "src/styleguide/docs-assets/**/*",
    },
    twig:                 "src/**/*.twig",
  },
  dist: {
    scss:      './dist/css/',
    files:     './dist/*',
    html:      './dist/',
    images:    './dist/images/',
    js:        './dist/js/',
    root:      './dist/',
  },
}

var jsFiles = {
  js: [ path.src.jsApp ]
}

path.src.static = [
  path.src.styleguide.css,
  path.src.styleguide.docsAssets,
  path.src.fonts,
  path.src.js,
];

function moveFiles( moveSrc, moveDist ) {
  return gulp.src( moveSrc )
    .pipe( gulp.dest( moveDist ) )
    .pipe( browsersync.stream() );
}

function clean(done) {
  return gulp.src(path.dist.files, {read: false})
    .pipe(cleanFiles());
  done();
} 

function cleanImages(done) {
  return gulp.src(path.dist.images, {read: false})
    .pipe(cleanFiles());
  done();
} 


function cleanJs(done) {
  return gulp.src(path.dist.js, {read: false})
    .pipe(cleanFiles());
  done();
} 

function static(done) {
  return gulp.src(path.src.static, {
    base: path.src.root
    })
    .pipe( gulp.dest( path.dist.root) )
    .pipe( browsersync.stream() );
  done();
} 

function reload(done) {
  browsersync.reload();
  done();
};

function browser_sync(done) {
  browsersync.init({
    server: {
      baseDir: path.server
    },
    injectChanges: true,
    open: false
  });
  done();
};


function images() {
  return gulp.src( path.src.imagesFolderFiles )
    .pipe( gulp.dest( path.dist.images ) )
    .pipe( browsersync.stream() );
};

function templates(done) {
  'use strict';
  return gulp.src( [path.src.twig, '!src/styleguide/includes/**']  )
    .pipe(twig({
        base: path.src.root,
        data: twigData,
    }))
    .pipe(rename({
        extname: '.html'
    }))
    .pipe(gulp.dest( path.dist.root ))
    .pipe( browsersync.stream() );
  done();
};




function scss() {
  return gulp.src( path.src.scss )
    .pipe( sourcemaps.init() )
    // Note this function is "scss" but the gulp variable is "sass"
    .pipe( sass({
      errorLogToConsole: true,
      outputStyle: 'compact'
    }))
    .on( 'error', console.error.bind( console ) )
    .pipe( autoprefixer({
      cascade: false
    }))
    // .pipe( rename({ suffix: '.min' }) )
    .pipe( sourcemaps.write( './' ) )
    .pipe( gulp.dest( path.dist.scss ) )
    .pipe( browsersync.stream() );
};

function es6(done) {
  jsFiles.js.map(function(entry) {
    return browserify({
      // make entry an array incase we have multiple later
      entries: [ path.src.jsRoot +  entry]
    })
    .transform( babelify, { presets: ['@babel/preset-env'] } )
    .bundle()
    .pipe( source( entry ) )
    .pipe( rename({ extname: '.js' }) )
    .pipe( buffer() )
    .pipe( sourcemaps.init({ loadMaps: true }) )
    // .pipe( uglify() )
    .pipe( sourcemaps.write( './' ) )
    .pipe( gulp.dest( path.dist.js ) )
    .pipe( browsersync.stream() );
  });
  done();
};

function watch_files(){
  gulp.watch(path.src.scss,   scss);
  gulp.watch(path.src.es6,    es6);
  gulp.watch(path.src.twig,   templates);
  gulp.watch(path.src.images, gulp.series(cleanImages, images));
  gulp.watch(path.src.static, static);
  
};


gulp.task('clean', clean);
gulp.task('scss', scss);
gulp.task('es6', es6);
gulp.task('images', images);
gulp.task('templates', templates);
gulp.task('static', static);
gulp.task('watch', watch_files);

gulp.task('build', gulp.series(clean, gulp.parallel(scss, images, es6, templates, static)));
gulp.task('default', gulp.series("build", gulp.parallel(watch_files, browser_sync)));

